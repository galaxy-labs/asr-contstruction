<?php
/**
 * The template for displaying the footer.
 */
?>
</div> <!-- c-wrapper end -->
<!--[if IE 8]>
<script>window.isMsIe = 8;</script><![endif]-->
<!--[if lte IE 7]>
<script>window.isMsIe = 7;</script><![endif]-->

<?php wp_footer(); ?>
</body>
</html>