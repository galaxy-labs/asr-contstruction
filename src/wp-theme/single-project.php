<?php
/**
 * Template Name: Project
 */

get_header('project'); ?>

    <div class="b-content b-content_project">
        <section class="b-slider-block">
            <div class="b-slider b-slider_project-page">
                <div class="b-slider-container b-slider-container_project-page">
                <?php
                if (have_posts()):
                    while (have_posts()):
                        the_post();
                        $gallery = get_post_meta(get_the_ID(), 'gallery');
                        if (isset($gallery) && !empty($gallery)):
                            $thumbs = array();
                            foreach ($gallery as $image):
                                $thumbs[] = array_shift(wp_get_attachment_image_src($image['ID'], 'project-thumbnail'));
                            ?>
                            <div class="b-slide b-slide__project" style="background-image: url(<?= array_shift(wp_get_attachment_image_src($image['ID'], 'full')); ?>)">
<!--                                <img src="--><?//= array_shift(wp_get_attachment_image_src($image['ID'], 'full')); ?><!--"  alt=""/>-->
                            </div>
                            <?php
                            endforeach;
                        endif;
                        ?>
                </div>
                <div class="b-slider-block__nav">
                    <a href="#" class="b-prev"></a>
                    <a href="#" class="b-next"></a>
                </div>
            </div>
        </section>
    </div>
    <footer class="b-footer b-footer_project">
        <div class="b-footer__project">
            <div class="b-footer__project-title">
                <span><?= get_post_meta($post->ID, 'location', true); ?></span>
                <p><?php the_title(); ?></p>
            </div>
            <span class="b-close">CLOSE</span>
            <div class="b-footer__project-btn">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
            <div class="b-footer__block">
            <?php
            if (!empty($thumbs)):
                foreach ($thumbs as $thumbnail):
            ?>
                <a href="#"><img src="<?= $thumbnail; ?>" alt=""/></a>
            <?php
                endforeach;
            endif; ?>
            </div>
        </div>
    </footer>
    <?php
        endwhile;
    endif;
    ?>
<?php get_footer('project'); ?>