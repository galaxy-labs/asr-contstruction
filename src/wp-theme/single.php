<?php
get_header(); ?>
    <section class="b-blog-article">
        <?php if ( have_posts() ) : ?>
            <?php while ( have_posts() ) : the_post(); ?>
                <div class="b-blog-article-image">
                    <?php
                        if ( has_post_thumbnail()) { // check if the post has a Post Thumbnail assigned to it.
                            the_post_thumbnail('blog-thumbnail');
                        }

                        else {
                            $uri = get_template_directory_uri();
                            echo  "<img src='$uri/images/blog/bg-blog-article.jpg' />";
                        }
                    ?>

                </div>
                <div class="b-blog-article__inner">
                    <?php
                    $referer = explode('/', $_SERVER['HTTP_REFERER']);
                    $count = count($referer);
                    $part = ($referer[$count - 1] == '') ? $referer[$count - 2] : $referer[$count - 1];

                    if ($part == 'blog') {
                        $back = '/' . implode('/', array_slice($referer, 3));
                    } else {
                        $back = '/';
                    }

                    ?>
                    <a class="b-back-btn" href="<?php echo $back; ?>">BACK</a>
                    <div class="b-blog-article-wrap">
                        <h2><?php the_title(); ?></h2>
                        <div class="b-article-info">
                            by
                            <span class="b-post-author">
                                <?php echo get_the_author(); ?>
                            </span>
                            Posted in
                            <span class="b-category">
                                <?php
                                    $category = get_the_category();
                                    echo $category[0]->cat_name;
                                ?>
                            </span>
                            on
                            <span class="b-post-date">
                                <?php echo get_the_date("F j, Y"); ?>
                            </span>
                        </div>
                        <section class="b-simple-content b-simple-content_blue">
                            <div class="b-text-wrap">
                                <?php the_content(); ?>
                            </div>
                        </section>
                    </div>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
    </section>
    <section class="c-blog">
        <div class="b-blog">
            <?php $query = new WP_Query( array( 'post_type' => 'post', 'orderby' => 'DESC', 'posts_per_page' => -1 ) ); ?>
            <?php if ( $query->have_posts() ) : ?>
                <?php while ( $query->have_posts() ) : $query->the_post();?>
                    <?php
                    $category = get_the_category();
                    $category = strtolower($category[0]->cat_name);
                    ?>
                    <figure class='b-blog__item  <?php echo "b-filter_".$category; ?>' >
                        <?php if ( has_post_thumbnail()) { // check if the post has a Post Thumbnail assigned to it.
                            the_post_thumbnail();
                        } ?>

                        <figcaption>
                            <h2><span><?php echo get_the_date("M,d"); ?></span><?php the_title(); ?>.</h2>
                            <?php the_excerpt(); ?><a href="<?php the_permalink(); ?>"></a>
                        </figcaption>
                    </figure>
                <?php endwhile; ?>
            <?php endif; ?>
            <?php wp_reset_query(); ?>

        </div>
    </section>
<?php get_footer(); ?>