<?php
/**
 * Template Name: Contact
 */

get_header(); ?>
    <?php
    if (have_posts()):
        while (have_posts()):
            the_post();
            ?>
            <div class="b-content b-content_contact">
                <?php if (has_post_thumbnail( $post->ID ) ): ?>
                    <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
                    <div class="b-block-top-img b-visible" style="background-image: url('<?php echo $image[0]; ?>')">

                    </div>
                <?php endif; ?>
                <div class="b-block-bottom-content b-block-bottom-content_contact">
                    <div class="b-block-contact b-visible">
                        <span>ASR Interiors </span>
                        <div class="b-block-contact__address">
                            <?= get_field('contact_info_left-side'); ?>
                        </div>
                        <div class="b-block-contact__phone">
                            <?= get_field('contact_info_right-side'); ?>
                        </div>
                    </div>
                    <h3 class="b-visible">contact</h3>
                    <div class="b-block-contact-form b-visible">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
    <?php
        endwhile;
    endif;
    ?>
<?php get_footer(); ?>