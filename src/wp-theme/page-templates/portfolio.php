<?php
/**
 * Template Name: Portfolio
 */

get_header(); ?>
    <div class="b-content b-content_portfolio">
        <section class="b-portfolio-list">
        <?php
        $args = array(
            'post_type'      => 'project',
            'posts_per_page' => -1,
        );

        $the_query = new WP_Query($args);
        if ($the_query->have_posts()):
            while ($the_query->have_posts()):
                $the_query->the_post();
        ?>
            <div class="b-portfolio-item b-visible">
                <?php if (has_post_thumbnail( $post->ID ) ): ?>
                    <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
                    <a href="<?= get_permalink(); ?>"><div class="b-portfolio-item__img" style="background-image: url('<?php echo $image[0]; ?>')"></div></a>
                <?php endif; ?>

                <div class="b-portfolio-item__text">
                    <a href="<?= get_permalink(); ?>"><span><?= get_post_meta($post->ID, 'location', true); ?></span></a>
                    <a href="<?= get_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
                    <a class="b-portfolio-item__text-img" href="<?= get_permalink(); ?>"></a>
                </div>
            </div>
        <?php
            endwhile;
        endif;
        wp_reset_query(); ?>
        </section>
    </div>


<?php get_footer(); ?>