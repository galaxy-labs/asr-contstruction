<?php
/**
 * Template Name: Splash-Page
 */

get_header('home'); ?>

    <div class="b-content b-content_splash">
        <section class="b-index-page">
            <div class="b-index-page__left">
                <div>
                    <span>welcome to ASR</span>
                    <p>PLEASE SELECT ONE</p>
                    <a href="<?php echo esc_url( home_url( '/home' ) ); ?>">INTERIORS</a>
                    <a href="#">CONSTRUCTION</a>
                </div>
            </div>
            <div class="b-index-page__right">
                <a href="<?php echo esc_url( home_url( '/home' ) ); ?>" class="b-block-top">
                    <div class="b-block-top__bg"></div>
                    <div class="b-block-top__link">
                        <span></span>
                    </div>
                </a>
                <a class="b-block-bottom" href="<?php echo esc_url( home_url( '/construction' ) ); ?>">
                    <div class="b-block-bottom__bg"></div>
                    <div class="b-block-bottom__link">
                        <span></span>
                    </div>
                </a>
            </div>
        </section>
    </div>

<?php get_footer('home'); ?>