<?php
/**
 * Template Name: About Us
 */

get_header(); ?>
    <?php
        if (have_posts()):
            while (have_posts()):
                the_post();
                ?>
                <div class="b-content b-content_about">
                    <section class="b-about-container">
<!--                        <div class="b-block-top-img b-block-top-img_about">-->
<!--                            --><?php ////if (has_post_thumbnail()) the_post_thumbnail('full'); ?>
<!--                        </div>-->
                        <?php if (has_post_thumbnail( $post->ID ) ): ?>
                            <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
                            <div class="b-block-top-img b-visible" style="background-image: url('<?php echo $image[0]; ?>')">

                            </div>
                        <?php endif; ?>
                        <div class="b-block-bottom-content b-visible">
                            <div class="b-block-bottom-content__left b-block-bottom-content__left_about">
                                <div class="b-block-bottom-content__left-top b-block-bottom-content__left-top_about" data-speed="2.0">
                                    <span><?= get_field('heading'); ?></span>
                                </div>
                                <div class="b-block-bottom-content__left-bottom b-block-bottom-content__left-bottom_about" data-speed="1.5">
                                    <?php the_content(); ?>
                                </div>
                            </div>
                            <div class="b-block-bottom-content__right">
                                <div class="b-block-bottom-content__right-top b-block-bottom-content__right-top_about" data-speed="1.3">
                                    <img src="<?= get_field('right-side_image'); ?>" alt="img">
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
    <?php
            endwhile;
        endif;
    ?>
<?php get_footer(); ?>