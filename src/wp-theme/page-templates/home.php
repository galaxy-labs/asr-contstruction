<?php
/**
 * Template Name: Home
 */

get_header(); ?>

        <div class="b-content b-content_home">
            <section class="b-slider-block">
                <div class="b-slider b-slider_home-page b-visible">
                    <div class="b-slider-container">
                    <?php
                    if (get_field('gallery')):
                        $descriptions = array();
                        while (has_sub_field('gallery')):
                    ?>
                        <div class="b-slide b-slide_home-page" style="background-image: url(<?= the_sub_field('image'); ?>)">
<!--                            <img src="--><?//= the_sub_field('image'); ?><!--" alt=""/>-->
                        </div>
                    <?php
                        $descriptions[] = get_sub_field('description');
                        endwhile;
                    endif;
                    ?>
                    </div>
                    <div class="b-slider-nav">
                        <div class="b-slider-nav__block">
<!--                        --><?php //foreach($descriptions as $description): ?>
<!--                            <h3>--><?//= $description; ?><!--</h3>-->
<!--                        --><?php //endforeach; ?>
                            <ul>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <section class="b-home-preview">
                <div class="b-heading-block b-visible">
                    <h2><?= get_field('content'); ?></h2>
                </div>
                <div class="b-home-preview__list">
                    <div class="b-home-preview__item b-visible">
                        <div class="b-home-preview__item-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/dump/item-3.jpg" alt="img"/>
                        </div>
                        <div class="b-home-preview__item-content">
                            <span>WE DELIVER</span>
                            <p>on budget</p>
                            <h3>AND ON TIME</h3>
                        </div>
                    </div>
                    <div class="b-home-preview__item b-visible">
                        <div class="b-home-preview__item-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/dump/item-2.jpg" alt="img"/>
                        </div>
                        <div class="b-home-preview__item-content">
                            <span>WE BELIEVE IN</span>
                            <p>delivered by skilled artisans</p>
                            <h3>IMPECCABLE QUALITY</h3>
                        </div>
                    </div>
                    <div class="b-home-preview__item b-visible">
                        <div class="b-home-preview__item-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/dump/Palazzo Del Mare lobby.jpg" alt="img"/>
                        </div>
                        <div class="b-home-preview__item-content">
                            <span>WE ARE</span>
                            <p>south florida’s premier</p>
                            <h3>LUXURY BUILDER</h3>
                        </div>
                    </div>
                </div>
            </section>
        </div>

<?php get_footer(); ?>