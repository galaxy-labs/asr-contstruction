<?php
/**
 * Template Name: Services
 */

get_header(); ?>
    <?php
    if (have_posts()):
        while (have_posts()):
            the_post();
            ?>
            <div class="b-content b-content_services">
                <section class="b-services-container">
<!--                    <div class="b-block-top-img b-block-top-img_services">-->
<!--                        --><?php ////if (has_post_thumbnail()) the_post_thumbnail('full'); ?>
<!--                    </div>-->
                    <?php if (has_post_thumbnail( $post->ID ) ): ?>
                        <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
                        <div class="b-block-top-img b-visible" style="background-image: url('<?php echo $image[0]; ?>')">

                        </div>
                    <?php endif; ?>
                    <div class="b-block-bottom-content b-visible">
                        <div class="b-block-bottom-content__left">
                            <div class="b-block-bottom-content__left-top" data-speed="2.0">
                                <span>services</span>
                                <ul>
                                <?php
                                if (get_field('services')):
                                    while (has_sub_field('services')):
                                ?>
                                    <li><?= the_sub_field('services_field'); ?></li>
                                <?php
                                    endwhile;
                                endif;
                                ?>
                                </ul>
                            </div>
                            <div class="b-block-bottom-content__left-bottom" data-speed="1.8">
                                <img src="<?= the_field('left-side_image'); ?>" alt="">
                            </div>
                        </div>
                        <div class="b-block-bottom-content__right">
                            <div class="b-block-bottom-content__right-top" data-speed="2.4">
                                <img src="<?= the_field('right-side_image'); ?>" alt="img">
                            </div>
                            <div class="b-block-bottom-content__right-bottom" data-speed="1.5">
                                <span>why ASR?</span>
                                <?= the_field('why_asr'); ?>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
    <?php
        endwhile;
    endif;
    ?>

<?php get_footer(); ?>