<?php
/**
 * The template for displaying the footer.
 */
?>


    <footer class="b-footer b-footer_splash">
        <div class="b-footer__block">
            <a href="#" class="b-footer__logo"></a>
            <div class="b-footer__contact">
                <p>2 NE 40th Street, Ste 304</p>
                <p>Miami, FL 33137</p>
                <p><a href="tel:3055341989">PH (305) 534-1989</a></p>
                <a href="#">web site terms of use </a>
                <a href="#">site credit</a>
            </div>
        </div>
    </footer>
    </div> <!-- c-wrapper end -->
<!--[if IE 8]>
<script>window.isMsIe = 8;</script><![endif]-->
<!--[if lte IE 7]>
<script>window.isMsIe = 7;</script><![endif]-->

<?php wp_footer(); ?>
</body>
</html>