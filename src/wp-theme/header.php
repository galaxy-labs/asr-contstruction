<?php
/**
 * The Header for our theme.
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"/>
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
    <link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.14&amp;sensor=false"></script>
    <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
    <![endif]-->
    <script type="text/javascript" src="http://fast.fonts.net/jsapi/af8d5c39-3b9a-4801-92a0-3b9502b3ca98.js"></script>
    <?php wp_head(); ?>
</head>

<body>
    <!--[if lt IE 7]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!-- Add your site or application content here -->
    <div class="b-preloader">
        <span></span>
    </div>
    <div class="c-wrapper">
        <header class="b-header">
            <div class="b-header__block-logo">
                <?php
                $args = array(
                    'post_type'      => 'sitelogo',
                    'posts_per_page' => 1,
                );

                $the_query = new WP_Query($args);
                if ($the_query->have_posts()):
                    while ($the_query->have_posts()):
                        $the_query->the_post();
                        ?>
                        <a href="<?php echo esc_url( home_url( '/home' ) ); ?>" class="b-logo"><img src="<?= pods_image_url(get_post_meta($post->ID, 'logo', true)); ?>" alt="logo"/></a>
                    <?php
                    endwhile;
                endif;
                wp_reset_query(); ?>
            </div>
            <div class="b-header__block-nav">
                <div class="b-header__button">
                    <a href="#">ASR CONSTRUCTION</a>
                    <ul class="b-header__button-languages">
                        <li><a href="">English</a></li>
                        <li><a href="">Spanish</a></li>
                        <li><a href="">Portuguese</a></li>
                        <li><a href="">Russian</a></li>
                        <li><a href="">Italian</a></li>
                        <li><a href="">French</a></li>
                    </ul>
                </div>
<!--                <ul>-->
<!--                    <li><a href="">PORTFOLIO</a></li>-->
<!--                    <li><a href="">SERVICES</a></li>-->
<!--                    <li><a href="">ABOUT US</a></li>-->
<!--                    <li><a href="">CONTACT</a></li>-->
<!--                </ul>-->
                <?php wp_nav_menu( array( 'theme_location' => 'primary', 'container' => false, 'menu_class' => 'b-menu') ); ?>
                <div class="b-button">
                    <span></span>
                </div>
            </div>
        </header>
        <div class="b-mobile-menu">
            <?php wp_nav_menu( array( 'theme_location' => 'primary', 'container' => false, 'menu_class' => 'b-menu') ); ?>
        </div>
