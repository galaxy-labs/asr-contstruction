<?php
/**
 * The template for displaying 404 pages (Not Found).
 */

get_header(); ?>
    <div class="b-content b-content_error">
        <section class="b-error-container">
            <div class="b-error-container__left">
                <p>404<em>Broken Link</em></p>
            </div>
            <div class="b-error-container__right">
                <p><em>we’re sorry.</em> the page you requested can not be found. </p>
            </div>
        </section>
    </div>
<?php get_footer(); ?>