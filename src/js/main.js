//$ = jQuery;
$(document).ready(function () {

	//Globbal variables
    var isMobile = (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)),
        $window = $(window),
        windowW = $(window).width(),
        windowH = $(window).height(),
        windowScrollTop = $window.scrollTop();

    function preloared() {
        $(window).load( function() {
            var preloader = $('.b-preloader'),
                spinner   = preloader.find('.spinner');

            spinner.fadeOut();
            preloader.delay(350).fadeOut('slow');
        })
    }

    function languages() {
        $('.b-header__button-languages').on('click', function(e) {
            e.preventDefault();
            $('.b-header__button-languages').toggleClass('open');
        })
    }

    var Slider = (function(){

        function myslider(slider) {
            var sliderLayout = slider.find('.b-slider-container'),
                slide = slider.find('.b-slide'),
                count = slide.length,
                rightArrow = slider.find('.b-next'),
                leftArrow = slider.find('.b-prev'),
                controlPanel = slider.find('.b-slider-nav__block ul'),
                isAnimate = false,
                methods;


            methods = {

                init2 : function() {
                    slider.currentSlide = 0;

                    if (controlPanel.length > 0) {
                        methods.makeControls();
                    }

                    if ($('.b-slider_home-page').length > 0 ){
                        methods.autoSwitch();
                    }

                    if ( $('.b-slider_project-page').length > 0 ) {
                        methods.autoSwitch();
                    }

                    slide.eq(3).clone().insertAfter(slide.last()).addClass('clone');
                    slide.eq(2).clone().insertAfter(slide.last()).addClass('clone');
                    slide.eq(1).clone().insertAfter(slide.last()).addClass('clone');
                    slide.eq(0).clone().insertAfter(slide.last()).addClass('clone');

                    slide = slider.find('.b-slide');

                    if (isMobile) {
                        slider.on( "swipeleft", function(){

                            if ( windowW < 1025) {
                                if (isAnimate) return false
                                slider.currentSlide = methods.slideRight(slider.currentSlide);
                            }
                        });

                        slider.on( "swiperight", function(){
                            if ( windowW < 1025) {
                                if (isAnimate) return false
                                slider.currentSlide = methods.slideLeft(slider.currentSlide);
                            }
                        });
                    }

                    rightArrow.on('click', function(){
                        if (isAnimate) return false
                        slider.currentSlide = methods.slideRight(slider.currentSlide);
                    });

                    leftArrow.on('click', function(){
                        if (isAnimate) return false
                        slider.currentSlide = methods.slideLeft(slider.currentSlide);
                    });


                    controlPanel.find('li').on('click', function(){
                        var index = $(this).index(),
                            heading = $('.b-slider-nav__block h3'),
                            itemLength = $('.b-slide_home-page.clone').length,
                            indexItem = $('.b-slide_home-page.active').index()-1;

                        if ( indexItem > itemLength) {
                            indexItem = indexItem - itemLength;
                        }

                        heading.removeClass('active');
                        heading.eq(index).addClass('active');

                        slider.currentSlide = index;
                        methods.goTo(slider.currentSlide);
                    });

                },

                slideRight: function(){
                    var index = $('.b-slide__project.active').index();

                    slide.removeClass('active');
                    slide.eq(index + 1).addClass('active');

                    isAnimate = true;
                    if (index === count) {
                        index = 0;
                        methods.changeSlide(index);
                    }

                    if (index < count ) {
                        index++;
                        methods.goTo(index);
                    }

                    return index;
                },

                slideLeft: function(){
                    var index = $('.b-slide__project.active').index();

                    isAnimate = true;
                    if (index === 0) {
                        index = count;
                    }
                    if (index > 0) {
                        index--;
                        methods.goTo(index);
                    }

                    return index;
                },

                goTo: function(index) {
                    var offsetLeft = slide.eq(index).position();

                    controlPanel.find('li').removeClass('active');
                    controlPanel.find('li').eq(index).addClass('active');
                    slide.removeClass('active');
                    slide.eq(index).addClass('active');

                    if (index === controlPanel.find('li').length) {
                        controlPanel.find('li').removeClass('active');
                        controlPanel.find('li').eq(0).addClass('active');
                    }

                    sliderLayout.stop(true, false).animate({'margin-left': -offsetLeft.left},500, function(){
                        isAnimate = false;
                    });
                },

                changeSlide: function(index) {
                    var offsetLeft = slide.eq(index).position();
                    sliderLayout.stop(true, false).css('margin-left', -offsetLeft.left);
                },

                resizeSlider: function() {

                },

                autoSwitch: function(){
                    window.onload = function (){
                        var intervalID;
                        var heading = $('.b-slider-nav__block h3');

                        heading.eq(0).addClass('active');
                        $('.b-slide_home-page').eq(0).addClass('active');

                        var start = function(){
                            intervalID = setInterval(function(){
                                 var itemLength = $('.b-slide_home-page.clone').length,
                                     indexItem = $('.b-slide_home-page.active').index() - 1;

                                if ( indexItem > itemLength) {
                                    indexItem = indexItem - itemLength;
                                }

                                slider.currentSlide = methods.slideRight(slider.currentSlide);
                                heading.removeClass('active');
                                heading.eq(indexItem).addClass('active');
                            }, 8000);
                        }
                        var stop = function(){

                            if(intervalID){
                                var itemLength = $('.b-slide_home-page.clone').length,
                                    indexItem = $('.b-slide_home-page.active').index();

                                if ( indexItem > itemLength) {
                                    indexItem = indexItem - itemLength;
                                }

                                heading.removeClass('active');
                                heading.eq(indexItem).addClass('active');
                                clearTimeout(intervalID);
                            }
                        }

                        $('.b-slider-block').mouseenter(function(){
                            stop();
                        });
                        $('.b-slider-block').mouseleave(function(){
                            start();
                        });

                        $('.b-footer__project-btn').mouseenter(function(){
                            stop();
                        });

                        $('.b-footer__project-btn').mouseleave(function(){
                            start();
                        });

                        $('.b-footer__block a').mouseenter(function(){
                            stop();
                        });

                        $('.b-footer__block a').mouseleave(function(){
                            start();
                        });

                        start();
                    }
                },

                makeControls: function() {
                    for (var i = 0; i < $('.b-slide_home-page').length; i++){
                        var sliderNav = $('<li></li>')
                        controlPanel.append(sliderNav);
                        $('.b-slider-nav__block ul > li').eq(0).addClass('active');
                    }
                }

            }

            methods.init2();
        }

        function init() {
            var slider = $('.b-slider');
            if (slider.length > 0) {
                $('.b-slider').each(function(){
                    var  slider = $(this);
                    myslider(slider);
                });
            }
        }


        return {
            init: init
        }
    })();

    var Parallax = (function(){

        var items = $('[data-speed]'),
            scrollTop = 0,
            inertiaScrollTop = 0,
            isStop = false,
            animationRequest,
            mouseX = 0,
            wrapperInertia,
            mouseY = 0,
            requestAnimFrame = (function() {
                return  window.requestAnimationFrame       ||
                    window.webkitRequestAnimationFrame ||
                    window.mozRequestAnimationFrame    ||
                    window.oRequestAnimationFrame      ||
                    window.msRequestAnimationFrame     ||
                    function( callback, element) {
                        window.setTimeout(callback, 1000 / 60);
                    };
            })();

        function buildParallax() {

            var currentScrollTop = 0;


            $window.scrollTop(0);

            items.each(function( indx ) {

                var element = $(this),
                    speed = element.data('speed');

                setDataAttributes(element);

                setStartPositions(element);

                currentScrollTop = scrollTop;

            });

            draw();

            function setDataAttributes(element) {
                element.data("height", element.innerHeight());
                element.data("offset-top", element.offset().top - $window.scrollTop());
                element.data("offset-x", 0);
                element.data("offset-y", 0);
            }

            function setStartPositions(element) {
                var speed = element.data('speed'),
                    height = element.data('height'),
                    offsetTop = element.data('offset-top'),
                    newOffsetY = currentScrollTop + windowH / 2 - height / 2 - offsetTop,
                    offsetX = 0,
                    offsetY = -1 * newOffsetY * speed / 20;

                element.data('offset-y', offsetY);

                element.css({
                    '-webkit-transform':'translate3d(0px, ' + offsetY + 'px, 0)',
                    '-moz-transform':'translate3d(0px, ' + offsetY + 'px, 0)',
                    'transform':'translate3d(0px, ' + offsetY + 'px, 0)'
                });

            }

        }

        function updateParallax() {
            var scrollDiff,
                containerTop,
                cTop;

            inertiaScrollTop += 0.01 * (scrollTop - inertiaScrollTop);
            scrollDiff = inertiaScrollTop - scrollTop;

            if (Math.abs(scrollDiff) < 10) {
                inertiaScrollTop = scrollTop;

                return false;
            }

            items.each(function() {
                var $item = $(this),
                    speed = $item.data('speed'),
                    height = $item.data('height'),
                    offsetTop = $item.data('offset-top'),
                    pageX,
                    pageY,
                    newOffsetX,
                    newOffsetY = inertiaScrollTop + windowH / 2 - height / 2 - offsetTop,
                    offsetY = -1 * newOffsetY * speed / 20;


                $item.data('offset-y', offsetY);

                $item.css({
                    '-webkit-transform':'translate3d(0px, ' + offsetY + 'px, 0)',
                    '-moz-transform':'translate3d(0px, ' + offsetY + 'px, 0)',
                    'transform':'translate3d(0px, ' + offsetY + 'px, 0)'
                });
            })

        }

        function draw() {

            animationRequest = requestAnimFrame( draw );

            if (isStop) {
                return;
            }

            updateParallax();

        }

        function stopParallax() {
            isStop = true;
        }

        if ( windowW < 768 ) {
            setTimeout( function() {
                $('[data-speed]').attr('style', ' ');
            }, 100);

            stopParallax();
        }

        $(window).resize(function() {
            windowW =  $(window).width();

            if ( windowW < 768 ) {
                setTimeout( function() {
                    $('[data-speed]').attr('style', ' ');
                }, 100);

                stopParallax();
            }
        })

        function parallaxResize() {
            if (windowW < 768) {
                isStop = true;
            }

            else {
                isStop = false;
                //buildParallax();
            }
        }

        function init() {

            var timeout;

            if  (isMobile ) {
                return;
            }

            buildParallax();

            $(document).on('scroll', function() {
                scrollTop = $(this).scrollTop();

            });

            $window.on('resize', function(){

                clearTimeout(timeout);

                timeout = setTimeout(parallaxResize, 100);

            });
        }

        return {
            init: init
        }
    })();

    function homeSlider() {
        var heading = $('.b-slider-nav__block h3');
    }

    function projectPage() {
        $window.on('load', function() {
            var projectSlider = $('.b-slider-container_project-page');
            projectSlider.css({'height': windowH - 116});
        });

        $window.resize( function() {
            var projectSlider = $('.b-slider-container_project-page'),
                windowH = $(window).height();
            projectSlider.css({'height': windowH - 116});
        });

        $('.b-footer__project-btn').on('click', function() {

            $('.b-footer__block').add('.b-close')
                .add('.b-slide__project').toggleClass('open');

            if ( $(window).scrollTop() == 24 ) {
                $(window).scrollTop(-24)
            }
        });

        $('.b-footer_project .b-footer__block a').on('click', function() {
            var itemIndex = $(this).index(),
                sliderItem =  $('.b-slide__project'),
                containerPositionStr = $('.b-slider-container_project-page').css('margin-left'),
                containerPosition = containerPositionStr.split('px')[0],
                itemPositionMargin = sliderItem.css('margin-left').split('px')[0],
                itemPosition = sliderItem.eq(itemIndex).offset().left - itemPositionMargin;


            sliderItem.eq(itemIndex).addClass('active').siblings().removeClass('active');
            $('.b-slider-container_project-page').stop(true, false).animate({'margin-left': containerPosition - itemPosition});
        });

        $('.b-slider_project-page').on('click', function() {
            $('.b-next').trigger('click');
        })
    }

    function mobileMenu() {
        $('.b-button').on('click', function() {
            var windowW = $(window).width(),
                windowH = $(window).height();

            $('.b-mobile-menu').toggleClass('open');
            $('.b-button').toggleClass('open');
            $('.b-header__block-logo').toggleClass('open');
            $('.b-header').toggleClass('open');

            if ( windowW > windowH ) {
                if ( $(window).scrollTop() == 0) {
                    $(window).scrollTop(150);
                }

                console.log(windowW > windowH)
            }
        });
    }

    function selectChosen() {
        if ( !isMobile ) {
            $('.wpcf7-select').chosen();
            $('.chosen-search').find('input').attr('readonly', '');

            $('.b-select-bock').on('click', function() {
                $('.chosen-results').toggleClass('open');
            });
        }
    }

    function waypointInit() {
        var waypointItem = $('.b-visible');

        setTimeout( function() {
            $('.b-home-preview__item').eq(0).addClass('b-waypoint');
        }, 100);

        waypointItem.waypoint({
            handler: function(direction) {
                var element = $(this.element);

                if (direction === 'down') {
                    element.addClass('b-waypoint');
                }
            },

            offset: '99%'
        })

        if ( $('html').hasClass('touch') ) {
            $('.b-visible').addClass('b-waypoint');
        }
    }

    function windowScroll() {
        $(window).on('scroll' , function() {
            var header = $('.b-header'),
                logo = $('.b-logo');

            if ( $(window).scrollTop() > 20)  {
                if ( !$('.b-content').hasClass('b-content_project') ) {
                    header.addClass('b-header_project');

                }else {
                    $('.b-footer__block').add('.b-close')
                        .add('.b-slide__project').addClass('open');

                    $('.b-slide img').css({'height': windowH - 184});
                }
            }else {
                if ( !$('.b-content').hasClass('b-content_project') ) {
                    header.removeClass('b-header_project');

                }else {
                    $('.b-footer__block').add('.b-close')
                        .add('.b-slide__project').removeClass('open');

                    $('.b-slide img').css({'height': ''});
                }
            }
        });
    }


    function globalInit() {
        languages();
        Slider.init();
        Parallax.init();
        homeSlider();
        projectPage();
        mobileMenu();
        preloared();
        selectChosen();
        waypointInit();
        windowScroll();
	}

	globalInit();


});

