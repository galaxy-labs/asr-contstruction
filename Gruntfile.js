/************************************************************************

!!! READ ME !!!

* Run "grunt" to run all tasks.

* Run "grunt watch" to monitor files and run tasks automatically.
    - Watched files: config files, scss, js, css, images
    - NOTE: Initialization of "grunt watch" will take several seconds

* Other grunt tasks:
    - "grunt clean": clean all dist folders
    - "grunt images": clean and generate all images
    - "grunt css": clean and generate all css
    - "grunt js": clean and generate all js

************************************************************************/

path = require('path');

developers = {
    //ilya: "/Applications/XAMPP/xamppfiles/htdocs/guidewire",
    ilyaB: "C:\\xampp\\htdocs\\asr-constraction\\wp-content\\themes\\asr-constraction\\"
}

server = {
    // Your hostname.
    host: 'www.mrktdev.com',

    // Default FTP port, leave this alone.
    port: 21,

    // Path to your destination folder, relative to the server root.
    dest: '/public_html/asr/construction/wp-content/themes/asr'
}


/*global module:false*/
module.exports = function(grunt) {

    paths = {
        dist: 'dist',
        source: 'src',
        bower: 'bower_components',
        temp: 'temp'
    };

    // if working locally
    if (grunt.option('developer')) {
        paths.dist = developers[grunt.option('developer')];
    }

    // Require modules
    // require('time-grunt')(grunt); // used to show task times
    require('jit-grunt')(grunt); // used to load grunt modules just in time (jit)

    /********************************
    ***** Project configuration *****
    ********************************/
    grunt.initConfig({
        // Load meta data from package.json
        pkg: grunt.file.readJSON('package.json'),

        // Setup banner, uses pkg data
        banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
            '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
            '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
            '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;' +
            ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %> */\n',


        /******************************
        ***** Tasks configuration *****
        ******************************/

        // Watch task (grunt watch)
        watch: {
            /* files: ['.csslintrc', '.jshintrc', 'Gruntfile.js', '.compass.rb', // config files
            ], */
            html: {
                files: [
                    'src/html/**/*.html'
                ],
                tasks: ['copy:html'],
                options: {
                    livereload: true
                }
            },
            compass: {
                files: [
                    'src/sass/**/*.scss'
                ],
                tasks: ['clean:css', 'compass:dist', 'copy:css', 'copy:sprites'],
                options: {
                    livereload: true
                }
            },
            js: {
                files: [
                    'src/js/**/*.js'
                ],
                tasks: ['clean:js', 'bower_concat', 'copy:jsdev' , 'copy:js', 'copy:modernizr'],
                options: {
                  livereload: true
                }
            },
            images: {
                files: [
                    'src/static/images/*.{png,jpg,gif,svg}'
                ],
                tasks: ['clean:images', 'copy:imagesdev'],
                options: {
                    livereload: true
                }
            },
            files: {
                files: [
                    'src/static/**',
                    '!src/static/images/*.{png,jpg,gif,svg}'
                ],
                tasks: ['clean:dist', 'copy'],
                options: {
                    livereload: true
                }
            },
            wordpress: {
                files: [
                    'src/wp-theme/**/*.php'
                ],
                tasks: ['copy:wordpress'],
                options: {
                    livereload: true
                }
            }
        },

        // Clean up tasks
        clean: {
            dist: ['dist'],
            images: ['dist/images'],
            css: ['dist/css'],
            js: ['dist/js']
        },

        // Minify SVG
        svgmin: {
            options: {},
            dist: {
                files: [{
                    expand: true,
                    cwd: 'src/static/images/',
                    src: ['*.svg'],
                    dest: 'temp/images',
                }]
            }
        },

        // Minify PNG, JPG, GIF images
        imagemin: {
            options: {
                optimizationLevel: 3,
                cache: false
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: 'src/static/images/',
                    src: ['*.{png,jpg,gif}'],
                    dest: 'temp/images',
                }]
            }
        },

        // Check JS syntax
        jshint: {
            options: {
                jshintrc: '.jshintrc',
                globals: {
                    jQuery: true
                },
                predef: ['Modernizr', 'window']
            },
            dist: {
                src: [/*'js/init.js',*/ 'src/js/main.js']
            }
        },

        // Compile sass via compass
        compass: {
            dist: {
                options: {
                    config: '.compass.rb'
                }
            }
        },

        // Check css syntax
        csslint: {
            options: {
                csslintrc: '.csslintrc'
            },
            dist: {
                src: ['temp/css/*.css']
            }
        },

        bower_concat: {
            all: {
                dest: path.join(paths.temp, 'js', 'plugins.js'),
                exclude: [
                    'modernizr'
                ],
                dependencies: {
                    'waypoints': 'jquery',
                },
                bowerOptions: {
                    relative: false
                },
                mainFiles: {
                    'waypoints': 'lib/jquery.waypoints.min.js'
                }
            }
        },

        // Concatenate js
        concat: {
            options: {
                banner: '<%= banner %>',
                stripBanners: true
            },
            dist: {
                src: ['src/js/plugins.js', 'src/js/main.js'],
                dest: 'temp/js/main.js'
            }
        },

        // Uglify/compress JS
        uglify: {
            options: {
                banner: '<%= banner %>'
            },
            dist: {
                src: '<%= concat.dist.dest %>',
                dest: 'temp/js/main.js'
            }
        },

        copy: {
            modernizr: {
                expand: true,
                cwd: path.join(paths.bower, 'modernizr'),
                src: ['modernizr.js'],
                dest: path.join(paths.dist, 'js')
            },
            html: {
                expand: true,
                cwd: path.join(paths.source, 'html'),
                src: ['**/*'],
                dest: paths.dist
            },
            wordpress: {
                expand: true,
                cwd: path.join(paths.source, 'wp-theme'),
                src: ['**/**'],
                dest: paths.dist
            },
            sprites: {
                expand: true,
                cwd: path.join(paths.temp, 'images'),
                src: ['**/sprites.png'],
                dest: path.join(paths.dist, 'images')
            },
            css: {
                expand: true,
                cwd: path.join(paths.temp, 'css'),
                src: ['**/*'],
                dest: path.join(paths.dist, 'css')
            },
            imagesdev: {
                expand: true,
                cwd: path.join(paths.source, 'static', 'images'),
                src: ['**/*'],
                dest: path.join(paths.temp, 'images')
            },
            images: {
                expand: true,
                cwd: path.join(paths.temp, 'images'),
                src: ['**/*'],
                dest: path.join(paths.dist, 'images')
            },
            jsdev: {
                expand: true,
                cwd: path.join(paths.source, 'js'),
                src: ['**/*'],
                dest: path.join(paths.temp, 'js')
            },
            js: {
                expand: true,
                cwd: path.join(paths.temp, 'js'),
                src: ['**/*'],
                dest: path.join(paths.dist, 'js')
            },
            files: {
                expand: true,
                cwd: path.join(paths.source, 'static'),
                src: ['**/*', '!**/images/**'],
                dest: paths.dist
            }
        },

        ftpush: {
            build: {
                auth: {
                  // Your hostname.
                  host: server.host,

                  // Default FTP port, leave this alone.
                  port: 21,

                  // Key name defined in `.ftppass` for your FTP account.
                  authKey: 'key1'
                },

                // Path to the folder you're interested in relative to `Gruntfile.js`.
                src: './dist',

                // Path to your destination folder, relative to the server root.
                dest: server.dest,

                // Files you don't want uploaded, relative to `Gruntfile.js`
                exclusions: [
                  '.DS_Store',
                  '.gitignore',
                  '.ftppass'
                ],
                simple: true
            }
        },

        'sftp-deploy': {
            build: {
                auth: {
                  // Your hostname.
                  host: server.host,

                  // Default FTP port, leave this alone.
                  port: 22,

                  // Key name defined in `.ftppass` for your FTP account.
                  authKey: 'key1'
                },
                cache: 'sftpCache.json',

                // Path to the folder you're interested in relative to `Gruntfile.js`.
                src: './dist',

                // Path to your destination folder, relative to the server root.
                dest: server.dest
            }
        },

        /************************************
        ***** Concurrency configuration *****
        ************************************/
        // Setup concurrent tasks, read from top-down in columns!
        concurrent: {
            //step1: ['svgmin:dist', 'imagemin:dist', 'bower_concat'],
            step1: ['copy:imagesdev', 'bower_concat', 'copy:html'],
            step2: ['jshint:dist', 'compass:dist'],
            step3: ['concat:dist', 'csslint:dist'],
            step4: ['uglify:dist'],
            step5: ['copy:modernizr', 'copy:wordpress', 'copy:css', 'copy:images', 'copy:js', 'copy:files'],
            options: {
                logConcurrentOutput: false // set to true to see task outputs
            }
        },

    });

    if (grunt.option('upload')) {
        watchTasks = grunt.config.get('watch');

        Object.keys(watchTasks).forEach(function(key) {
            var task = watchTasks[key];
            task.tasks.push('upload');
        });
        
        grunt.config.set('watch', watchTasks);
    } else {
        //grunt.config.set(grunt.config.get([prop]), value)
    }

    /*************************
    ***** REGISTER TASKS *****
    *************************/

    //upload task
    if (server.port == 21) {
        grunt.registerTask('upload', ['ftpush:build']);
    } else if (server.port == 22) {
        grunt.registerTask('upload', ['sftp-deploy:build']);
    }

    grunt.registerTask('release', [
        'clean',
        'concurrent:step1',
        'concurrent:step2',
        'concurrent:step3',
        'concurrent:step4',
        'concurrent:step5',
        'upload'
    ]);

    // All other tasks (such as local tasks) can be ran here
    grunt.registerTask('default', ['concurrent:step1', 'concurrent:step2', 'concurrent:step3', 'concurrent:step4']);


    // grunt.registerTask('default', ['concurrent:step2','concurrent:step3']);
    grunt.registerTask('images', ['clean:images', 'svgmin:dist', 'imagemin:dist']);
    grunt.registerTask('css', ['clean:css', 'compass:dist', 'csslint:dist']);
    grunt.registerTask('js', ['clean:js', 'jshint:dist', 'concat:dist', 'uglify:dist']);
    // grunt.registerTask('js', ['clean:js', 'jshint:dist', 'concat:dist']);



};